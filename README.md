# Java Chat with RMI

This is a sample code to showcase the usage of Java RMI. It provides a simple text chat server and clients, as well as the interaction needed to establish an "audio call". Audio calling itself is not implemented, as it was out of the scope of the demo presented at the time.

The server exports a IChatServer interface that clients use to connect and interact with the server and start new chats. Clients export a IChatClientRemote interface that allows the server and other clients to interact with it.

A client first connects with a server (IChatServer.connectClient). A reference to it's IChatClientRemote is passed along, so the server can call back on the client and pass the reference to other clients.

After a successful connection, the client requests the list of connected peers via the IChatServer.getClients function. To chat with a specific client, it must get a reference to the peer's IChatClientremote instance. It does so via the getRemotePeer method.

To start a chat, the caller executes the `wannaChat` method on the callee. This method exchanges IChatSession references between caller and callee -- so each one of them can call the other's `gotMessage` method.

"Audio Calls" follow a similar pattern -- caller and callee exchange IOngoingCall references and either stabilish an ongoing call via the `accept()` method or reject them via the `reject()` function.

# Issues

- The server does not have any kind of authentication -- neither it's possible with the current API.
- There's no crypto -- although it could be added easily via a SocketFactory implementing SSL
- Thread-safety was not examined in depth.
