package org.kzk.chat.server;

import  org.kzk.chat.common.ClientItem;
import org.kzk.chat.common.IChatServer;
import org.kzk.chat.common.IChatClientRemote;

import java.net.URI;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;

import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.*;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Server implements IChatServer {

    class ServerClientItem {
        String name;
        public IChatClientRemote remote;
        public ScheduledFuture<?> pingerHandle;

        public ServerClientItem(String name,
                                IChatClientRemote clientRemote,
                                ScheduledFuture<Void> pingerHandle)
        {
            this.name = name;
            this.remote = clientRemote;
            this.pingerHandle = pingerHandle;
        }
    }

    public HashMap<String, ServerClientItem> clients;
    public ScheduledExecutorService scheduller;

    public Server()
    {
        clients = new HashMap<>();
        scheduller = Executors.newScheduledThreadPool(1);

        scheduller.scheduleAtFixedRate(
                () -> displayPeerList(), 10, 10, SECONDS);
    }

    public void notifyPeerConnected(String peerName)
    {
        System.out.println("Notify of new client: " + peerName);
        synchronized (clients)
        {
            for (ServerClientItem client : clients.values()) {
                if (client.name.equals(peerName))
                    continue;
                scheduller.submit(
                        () -> {
                            try {
                                System.out.println("Notify " + client.name + " of client " + peerName + " entering");
                                client.remote.onPeerConected(peerName);
                            } catch (RemoteException e) {
                                System.out.println("Error notifying " + client.name + " : " + e);
                            }
                        });
            }
        }

    }

    public void notifyPeerDisconnected(String peerName)
    {

        synchronized (clients)
        {
            for (ServerClientItem client : clients.values()) {
                if (client.name.equals(peerName))
                    continue;
                scheduller.submit(() -> {

                    System.out.println("Notify " + client.name + " of client " + peerName + " leaving");
                    try {
                        client.remote.onPeerDisconnected(peerName);
                    } catch (RemoteException e) {
                        System.out.println("Error notifying " + client.name + " : " + e);
                    }

                });
            }
        }
    }

    public void displayPeerList()
    {
        Collection<ServerClientItem> clientsValues = null;
        synchronized (clients)
        {
            clientsValues = clients.values();
        }

        StringBuffer output = new StringBuffer();
        output.append("Clients: \n");

        for (ServerClientItem client : clientsValues)
            output.append("\t" + client.name + "\n");


        System.out.println(output.toString());
    }

    boolean pingClient(ServerClientItem client)
    {
        try
        {
            int ret = client.remote.ping();
            System.out.println("Client  " + client.name + " still active");
        }
        catch (RemoteException e)
        {
            // Cancel the schedulled pinger
            client.pingerHandle.cancel(true);

            System.out.println("Client " + client.name + " is no longer responding");
            synchronized (clients)
            {
                // Remove the item from clients
                if (clients.containsKey(client.name)) {
                    clients.remove(client.name);
                    notifyPeerDisconnected(client.name);
                }
            }

            return false;
        }

        return true;
    }

    public boolean connectClient(String peerName, IChatClientRemote clientRemote ) throws RemoteException
    {
        synchronized (clients)
        {
            if (clients.containsKey(peerName))
                return false;

            System.out.println("New Client: " + peerName);
            notifyPeerConnected(peerName);

            ServerClientItem client = new ServerClientItem(
                    peerName,
                    clientRemote,
                    null
                    );

            clients.put(peerName, client);

            // Schedulle to ping every 10 seconds
            client.pingerHandle = scheduller.scheduleAtFixedRate(
                    () -> pingClient(client), 10, 10, SECONDS);


        }

        return true;
    }

    public void disconnectClient(String peerName) throws RemoteException
    {
        synchronized (clients)
        {
            if (clients.containsKey(peerName))
            {
                ServerClientItem client = clients.get(peerName);
                client.pingerHandle.cancel(true);
                clients.remove(peerName);

                notifyPeerDisconnected(peerName);
            }
        }
    }


    public ArrayList<String> getClients() throws RemoteException
    {
        ArrayList<String> result = new ArrayList<>();

        Collection<ServerClientItem> clientsValues = null;
        synchronized (clients) {
            clientsValues = clients.values();
        }

        for (ServerClientItem client : clientsValues)
        {
            ClientItem ci = new ClientItem(client.name, client.remote);
            result.add(ci.name);
        }

        return result;
    }

    public IChatClientRemote getRemotePeer(String peerName) throws RemoteException
    {
        synchronized (clients)
        {
            ServerClientItem sci = clients.getOrDefault(peerName, null);
            if (sci != null)
                return sci.remote;

            return null;
        }
    }

    public void runServer()
    {
        final String name = "ChatServer";
        final int serverPort = 2000;
        Registry registry = null;
        URL policyFile = getClass().getResource("/open.policy");

        System.out.println("Policy URI : " + policyFile);
        System.setProperty("java.security.policy", policyFile.toExternalForm());
        // Force a new security server with the specified policy
        System.setSecurityManager(new SecurityManager());
        if (System.getSecurityManager() == null) {
            System.out.println("Could not set a policy for the server. Quitting");
            System.exit(1);
        }

        try {
            IChatServer stub =
                    (IChatServer) UnicastRemoteObject.exportObject(this, 0);


            registry = LocateRegistry.createRegistry(serverPort);
            registry.rebind(name, stub);

            System.out.println("Chat Server Bound");

        } catch (Exception e) {
            System.err.println("Exception:");
            e.printStackTrace();
        }

    }
    public static void main(String[] args)
    {
        Server server = new Server();
        server.runServer();


    }

}
