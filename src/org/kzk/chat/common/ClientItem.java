package org.kzk.chat.common;

import java.io.Serializable;

public class ClientItem implements Serializable {
    public String name;
    public IChatClientRemote remote;

    public ClientItem(String name, IChatClientRemote clientRemote)
    {
        this.name = name;
        this.remote = clientRemote;
    }
}