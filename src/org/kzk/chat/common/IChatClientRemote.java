package org.kzk.chat.common;

import java.rmi.*;

public interface IChatClientRemote extends Remote {
    int ping() throws RemoteException;

    IChatSession wannaChat(String from, IChatSession remoteChatSession) throws RemoteException;
    IOngoingCall wannaCall(String from, IOngoingCall call) throws RemoteException;

    void onPeerConected(String peer) throws RemoteException;
    void onPeerDisconnected(String peer) throws RemoteException;
}
