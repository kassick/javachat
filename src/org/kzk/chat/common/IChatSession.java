package org.kzk.chat.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IChatSession extends Remote {
    void gotMessage(String message) throws RemoteException;
    void peerLeft() throws RemoteException;
}
