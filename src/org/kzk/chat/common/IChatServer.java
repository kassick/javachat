package org.kzk.chat.common;

import java.rmi.*;
import java.util.ArrayList;

public interface IChatServer extends Remote {
    boolean connectClient(String peerName, IChatClientRemote client) throws RemoteException;
    void disconnectClient(String peerName) throws RemoteException;
    ArrayList<String> getClients() throws RemoteException;
    IChatClientRemote getRemotePeer(String peerName) throws RemoteException;
}
