package org.kzk.chat.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IOngoingCall extends Remote {
    void accept() throws RemoteException;
    void reject() throws RemoteException;
    void hangup() throws RemoteException;
}
