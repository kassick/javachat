package org.kzk.chat.client;

import java.util.ArrayList;

public interface IDisconnectable {
    void disconnect();
}
