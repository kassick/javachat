package org.kzk.chat.client;

import org.kzk.chat.common.*;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.lang.reflect.Array;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.lang.System.out;

public class Client implements IChatClientRemote {

    int nextPing = 0;
    Registry registry;
    IChatClientRemote stub = null;
    ChatListWindow listWindow;
    IChatServer server;
    String myPeerName;


    public final  ArrayList<IDisconnectable> liveSessions = new ArrayList<>();

    public ScheduledExecutorService scheduller = Executors.newScheduledThreadPool(1);

    public void registerSession(IDisconnectable session) {
        synchronized (liveSessions) {
            if (!liveSessions.contains(session))
                liveSessions.add(session);
        }
    }

    public void unregisterSession(IDisconnectable session) {
        synchronized (liveSessions) {
            if (liveSessions.contains(session))
                liveSessions.remove(session);
        }
    }

    public int ping() throws RemoteException
    {
        System.out.println("Replying ping " + nextPing);
        return nextPing++;
    }

    public IChatSession wannaChat(String from, IChatSession remoteChatSession) throws RemoteException
    {
        ChatWindow chatWindow = ChatWindow.createFor(this, from);
        chatWindow.setRemoteSession(remoteChatSession);

        IChatSession session = (IChatSession)UnicastRemoteObject.exportObject(chatWindow, 0);

        if (session != null)
            chatWindow.doDisplay();

        return session;
    }

    public IOngoingCall wannaCall(String from, IOngoingCall call) throws RemoteException
    {
        OngoingCallWindow ongoingCall = OngoingCallWindow.createForCallee(this, call, from);

        if (ongoingCall != null)
            return ongoingCall.localCall;

        registerSession(ongoingCall);

        return null;
    }

    public void onPeerConected(String peer) throws RemoteException
    {
        System.out.println("New Peer : " + peer);
        scheduller.submit(
                () -> {
                    synchronized(listWindow.peerListModel) {
                        if (!listWindow.peerListModel.contains(peer))
                            listWindow.peerListModel.addElement(peer);
                    }
                });
    }

    public void onPeerDisconnected(String peer) throws RemoteException
    {
        System.out.println("Leaving : " + peer);
        scheduller.submit(
                () -> {
                    synchronized(listWindow.peerListModel) {
                        if (listWindow.peerListModel.contains(peer))
                            listWindow.peerListModel.removeElement(peer);
                    }
                });
    }

    public void doConnect(String nick, String addr)
    {
        myPeerName = nick;

        try {
            String name = "ChatServer";
            registry = LocateRegistry.getRegistry(addr, 2000);
            this.server = (IChatServer) registry.lookup(name);

            this.stub = (IChatClientRemote) UnicastRemoteObject.exportObject(this, 0);

            if (!server.connectClient(nick, stub))
            {
                System.out.println("Nick is already taken");
                return;
            }

            listWindow = ChatListWindow.createFor(this);
            listWindow.setConnectedServer(addr);

            scheduller.submit(
                    () -> {
                        ArrayList<String> peers = null;
                        listWindow.peerListModel.clear();
                        try {
                            peers = server.getClients();
                        }
                        catch (RemoteException e)
                        {
                            listWindow.setConnectedServer("Desconectado");
                            return;
                        }

                        for(String peer : peers)
                        {
                            listWindow.peerListModel.addElement(peer);
                        }

                        System.out.println("Atualizou!");
                    }
            );


        } catch (Exception e) {
            System.err.println("KVServer error");
            e.printStackTrace();
        }

    }

    void doDisconnect(boolean hide)
    {
        ArrayList<IDisconnectable> items = null;
        synchronized (liveSessions) {
            items = (ArrayList<IDisconnectable>) liveSessions.clone();
        }

        for (IDisconnectable session : items)
        {
            session.disconnect();
            unregisterSession(session);
        }

        try {
            server.disconnectClient(myPeerName);
        } catch (RemoteException e)
        {
        }

    }

    void doQuit() {
        doDisconnect(true);
    }

    public void doChatWith(String peerName)
    {
        IChatClientRemote peerRemote = null;
        IChatSession
                session = null,
                remoteSession = null;

        ChatWindow chat = ChatWindow.createFor(this, peerName);

        try {
            session = (IChatSession)UnicastRemoteObject.exportObject(chat, 0);
        } catch (RemoteException e) {
            System.out.println("Can not chat with " + peerName);
            return;
        }

        try {
            peerRemote = server.getRemotePeer(peerName);
            remoteSession = peerRemote.wannaChat(myPeerName, session);
        } catch (RemoteException e)
        {
            System.out.println("Could not start chat with " + peerName + ": " + e);
            return;
        }

        if (remoteSession != null)
        {
            registerSession(chat);

            chat.setRemoteSession(remoteSession);
            chat.doDisplay();
        } else {
            System.out.println("Remote session is null");
        }
    }

    public static void main(String[] args)
    {
        Client client = new Client();
        ClientServerSelect serverWindow = new ClientServerSelect(client);
        serverWindow.doShow();
        out.println("Main do Cliente");
    }
}
