package org.kzk.chat.client;

import org.kzk.chat.common.IOngoingCall;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class OngoingCallWindow implements IOngoingCall, IDisconnectable {
    public static final int
            STATUS_CALLER_RINGING = 0,
            STATUS_CALLEE_RINGING = 1,
            STATUS_ONGOING = 2,
            STATUS_HANGUP = -1;


    private JButton aceitarButton;
    private JButton rejeitarButton;
    private JPanel callerPanel;
    private JPanel calleePanel;

    private JButton cancelarButton;
    private JButton encerrarButton;
    private JLabel statusLabel;
    private JLabel peerNameLabel;
    private JPanel contentPane;
    private JPanel ongoingPanel;

    public IOngoingCall localCall, remoteCall;
    private String peerName;
    private JFrame frame;
    private int status;
    private Client client;

    public OngoingCallWindow() {
        encerrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                doHangup("Chamada Encerrada");
            }
        });
        aceitarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                doAcceptCall();
            }
        });
        rejeitarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                doRejectCall();
            }
        });
        cancelarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                doHangup("Chamada Cancelada");
            }
        });
    }

    @Override
    public void disconnect()
    {
        doHangup("Disconnected");
    }

    public void doCloseWindow() {
        client.unregisterSession(this);
        doHangup("janela fechada");
    }

    public static OngoingCallWindow createForCaller(Client client, String peerName) {
        OngoingCallWindow window = new OngoingCallWindow();
        window.client = client;
        window.frame = new JFrame("Ligação com " + peerName);
        window.frame.setContentPane(window.contentPane);
        window.frame.pack();
        window.frame.setSize(400, 400);
        window.peerName = peerName;
        window.statusLabel.setText("Chamando");
        window.peerNameLabel.setText(peerName);
        window.calleePanel.setVisible(false);
        window.callerPanel.setVisible(true);
        window.ongoingPanel.setVisible(false);

        window.frame.setVisible(true);

        IOngoingCall stub = null;
        try {
            stub = (IOngoingCall) UnicastRemoteObject.exportObject(window, 0);
        } catch (RemoteException e) {
            window.localCall = window.remoteCall = null;
            window.doHangup("Erro ao chamar ");
            e.printStackTrace();
            return null;
        }

        window.localCall = stub;
        window.remoteCall = null;
        window.status = STATUS_CALLER_RINGING;

        return window;
    }

    public static OngoingCallWindow createForCallee(Client client, IOngoingCall callerCall, String peerName) {
        OngoingCallWindow window = new OngoingCallWindow();
        window.client = client;
        window.frame = new JFrame("Ligação com " + peerName);
        window.frame.setContentPane(window.contentPane);
        window.frame.pack();
        window.frame.setSize(400, 400);
        window.statusLabel.setText("Recebendo chamada de");
        window.peerNameLabel.setText(peerName);
        window.peerNameLabel.setText(peerName);
        window.calleePanel.setVisible(true);
        window.callerPanel.setVisible(false);
        window.ongoingPanel.setVisible(false);


        window.frame.setVisible(true);

        IOngoingCall stub = null;
        try {
            stub = (IOngoingCall) UnicastRemoteObject.exportObject(window, 0);
        } catch (RemoteException e) {
            window.remoteCall = window.localCall = null;
            window.doHangup("Erro exportando objeto");
            return null;
        }

        window.localCall = stub;
        window.remoteCall = callerCall;
        window.peerName = peerName;
        window.status = STATUS_CALLEE_RINGING;

        return window;
    }

    public void doCall(Client remoteClient) {
        //this.callerPanel.setVisible(true);
        //this.calleePanel.setVisible(false);
        //this.ongoingPanel.setVisible(false);
        try {
            this.remoteCall = remoteClient.wannaCall(remoteClient.myPeerName, this.localCall);
        } catch (RemoteException e) {
            localCall = remoteCall = null;
            doHangup("Erro chamando");
        }
    }

    public void doAcceptCall() {
        // Executed on callee, by accept button click

        this.callerPanel.setVisible(false);
        this.calleePanel.setVisible(false);
        this.ongoingPanel.setVisible(true);

        try {
            remoteCall.accept(); // remote-call to caller
            this.status = STATUS_ONGOING;
            this.statusLabel.setText("Ligação em andamento com ");

        } catch (RemoteException e) {
            this.localCall = null;
            this.remoteCall = null;

            doHangup("Erro"); // hide elements
        }
    }

    public void doRejectCall() {
        this.callerPanel.setVisible(false);
        this.calleePanel.setVisible(false);
        this.ongoingPanel.setVisible(true);

        try {
            this.remoteCall.reject(); // remote-call to caller

            this.localCall = null;
            this.remoteCall = null;

            doHangup("Chamada Rejeitada");
        } catch (RemoteException e) {
            this.localCall = null;
            this.remoteCall = null;
            doHangup("Erro");
        }

    }

    public void doHangup(String statusmsg) {
        if (this.status != STATUS_HANGUP) {
            if (remoteCall != null)
                try {
                    remoteCall.hangup();
                } catch (RemoteException e) {
                    statusmsg = "ERRO desligando com ";
                }
        }

        remoteCall = null;
        localCall = null;

        this.status = STATUS_HANGUP;

        this.statusLabel.setText(statusmsg);
        this.encerrarButton.setEnabled(false);
        this.aceitarButton.setEnabled(false);
        this.rejeitarButton.setEnabled(false);
        this.cancelarButton.setEnabled(false);

        client.unregisterSession(this);
    }

    // Remote Methods
    @Override
    public void accept() throws RemoteException {
        // Executed by the caller, called remotely by the callee

        this.status = STATUS_ONGOING;
        this.ongoingPanel.setVisible(true);
        this.callerPanel.setVisible(false);
        this.calleePanel.setVisible(false);


        this.statusLabel.setText("Chamada em andamento com ");
    }

    @Override
    public void reject() throws RemoteException {
        // executed by the caller, called remotely by the callee

        this.ongoingPanel.setVisible(true);
        this.callerPanel.setVisible(false);
        this.calleePanel.setVisible(false);
        doHangup("Chamada Rejeitada");
    }

    @Override
    public void hangup() throws RemoteException {

        remoteCall = null;

        if (status == STATUS_CALLER_RINGING)
            doHangup("Chamada Rejeitada por");
        else if (status == STATUS_CALLEE_RINGING)
            doHangup("Chamada Cancelada por");
        else if (status == STATUS_ONGOING)
            doHangup("Chamada Encerrada");
        else
            doHangup("OIQ COMOFAZ/");
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        statusLabel = new JLabel();
        statusLabel.setText("Chamando");
        panel1.add(statusLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        panel1.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        peerNameLabel = new JLabel();
        peerNameLabel.setText("Fulano");
        panel1.add(peerNameLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer2 = new com.intellij.uiDesigner.core.Spacer();
        panel1.add(spacer2, new com.intellij.uiDesigner.core.GridConstraints(0, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer3 = new com.intellij.uiDesigner.core.Spacer();
        contentPane.add(spacer3, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        calleePanel = new JPanel();
        calleePanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(calleePanel, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        aceitarButton = new JButton();
        aceitarButton.setText("Aceitar");
        calleePanel.add(aceitarButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer4 = new com.intellij.uiDesigner.core.Spacer();
        calleePanel.add(spacer4, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        rejeitarButton = new JButton();
        rejeitarButton.setText("Rejeitar");
        calleePanel.add(rejeitarButton, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        callerPanel = new JPanel();
        callerPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(callerPanel, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        cancelarButton = new JButton();
        cancelarButton.setText("Cancelar");
        callerPanel.add(cancelarButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        ongoingPanel = new JPanel();
        ongoingPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(ongoingPanel, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        encerrarButton = new JButton();
        encerrarButton.setText("Encerrar");
        ongoingPanel.add(encerrarButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }
}
